package visual;

public class Wordle {

	private VentanaMenu ventanaMenu;
	private VentanaJuego ventanaJuego;

	public Wordle() {
		initialize();
	}

	private void initialize() {
		ventanaMenu = new VentanaMenu();
		ventanaJuego = new VentanaJuego();
		
		ventanaMenu.agregarVentanaSig(ventanaJuego);
		ventanaJuego.agregarVentanaSig(ventanaMenu);
	}
	
	public void jugar() {
		ventanaMenu.setVisible(true);
	}
}
