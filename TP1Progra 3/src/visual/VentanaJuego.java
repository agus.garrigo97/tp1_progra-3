package visual;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controlador.ControllerWordle;
import recurso.Dificultad;

@SuppressWarnings("serial")
public class VentanaJuego extends Ventana {

	private JTextField textFieldRespuesta;
	private JTextField[][] rtasLetras;
	private Integer rtasPosHorizontal;
	private Integer rtasPosVertical;
	private Integer buttonSize;
	private Integer avance;
	private Integer avanceVert;
	private String palabra;
	private ControllerWordle controller;
	private JLabel intentosRestantes;

	public VentanaJuego() {
		controller = new ControllerWordle();
		crearFrame();
	}

	protected void crearFrame() {
		setTitle("Wordle");
		setBounds(100, 100, 500, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		textFieldRespuesta = new JTextField();
		textFieldRespuesta.setBounds(162, 21, 96, 19);
		getContentPane().add(textFieldRespuesta);
		textFieldRespuesta.setColumns(10);
		
		JLabel lblInfoIngresar = new JLabel("Ingrese una palabra:");
		lblInfoIngresar.setBounds(32, 24, 120, 13);
		getContentPane().add(lblInfoIngresar);
		
		JLabel lblIntentos = new JLabel("Intentos restantes: ");
		lblIntentos.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblIntentos.setBounds(32, 309, 118, 13);
		getContentPane().add(lblIntentos);
		
		intentosRestantes = new JLabel("");
		intentosRestantes.setFont(new Font("Tahoma", Font.PLAIN, 12));
		intentosRestantes.setBounds(147, 310, 45, 13);
		getContentPane().add(intentosRestantes);
		
		crearBotonSonido();
	}
	
	public void jugar(Dificultad dificultadElegida) {
		controller.Juego(dificultadElegida);
		buttonSize = 30;
		rtasPosHorizontal = 32;
		rtasPosVertical = 65;
		avance = 40;
		avanceVert = 0;
		rtasLetras = new JTextField[controller.intentosTotales()][controller.getLargoDePalabra()];
		palabra = "";	
		mostrarIntentos();
		limpiarRta();
		
		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				procesarRta();
			}
		});
		btnOk.setBounds(268, 20, 55, 21);
		getContentPane().add(btnOk);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 47, 486, 13);
		getContentPane().add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(0, 278, 476, 13);
		getContentPane().add(separator_1);
		
		JButton btnVolver = new JButton("Volver Al Menu");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(getContentPane(), "No se guardara la partida actual");
				finalizarPartida();
			}
		});
		btnVolver.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnVolver.setBounds(347, 301, 105, 28);
		getContentPane().add(btnVolver);
	}
	
	private void procesarRta() {
		palabra = textFieldRespuesta.getText();
		controller.guardarRespuesta(palabra);
		if(!controller.rtaValida()) {
			JOptionPane.showMessageDialog(getContentPane(), "La longitud es incorrecta! La logitud de la palabra debe ser: " + controller.getLargoDePalabra() 
			+ "\n tu respuesta tiene longitud: " + palabra.length());
		}
		
		controller.guardarRespuesta(palabra);
		controller.jugar();
		
		JTextField[] letras = getCajasLetra();
		Color[] coloresxLetra = controller.coloresDeRta();
		if (controller.rtaValida()) {
			for (int i = 0; i < controller.getLargoDePalabra(); i++) {
				letras[i] = new JTextField();
				letras[i].setBackground(coloresxLetra[i]);
				letras[i].setText(palabra.charAt(i) + "");
				letras[i].setHorizontalAlignment(SwingConstants.CENTER);
				letras[i].setFont(new Font("Tahoma", Font.PLAIN, 15));
				letras[i].setColumns(10);
				letras[i].setBounds(rtasPosHorizontal + (avance*i), rtasPosVertical + avanceVert, buttonSize, buttonSize);
				getContentPane().add(letras[i]);		
			}
			
			
			if (controller.resultadoFinal()) {
				JOptionPane.showMessageDialog(getContentPane(), "Felicidades!! Ganaste");
				finalizarPartida();
			}
			if (!controller.existenIntentos()) {
				JOptionPane.showMessageDialog(getContentPane(), "No quedan intentos :(\n   Perdiste");
				finalizarPartida();
			}
			
			limpiarRta();
			mostrarIntentos();
			actualizarPantalla();
			avanceVert += 40;
		}
	}
	
	private void mostrarIntentos() {
		intentosRestantes.setText(controller.intentos() + "");
	}
	
	private void limpiarRta() {
		textFieldRespuesta.setText(null);
		textFieldRespuesta.requestFocus();
	}
	
	private void finalizarPartida() {
		cambiarVentana();
		borrarRtas();
	}
	
	private void borrarRtas() {
		for (int i = 0; i < rtasLetras.length; i++) {
			for (int j = 0; j < rtasLetras[i].length; j++) {
				if(rtasLetras[i][j] != null) {
					rtasLetras[i][j].setVisible(false);
				}
			}
		}
		rtasLetras = null;
	}
	
	private JTextField[] getCajasLetra() {
		return rtasLetras[controller.intentosTotales() - controller.intentos()];
	}
}
