package visual;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import recurso.Dificultad;

@SuppressWarnings("serial")
public class VentanaMenu extends Ventana {

	public VentanaMenu() {
		super();
		crearFrame();
	}
	
	
	protected void crearFrame() {	
		setTitle("Wordle");
		setBounds(100, 100, 500, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblTitulo = new JLabel("Wordle");
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setFont(new Font("Comic Sans MS", Font.BOLD, 25));
		lblTitulo.setForeground(new Color(255, 140, 0));
		lblTitulo.setBounds(191, 60, 93, 45);
		contentPane.add(lblTitulo);
		
		JButton btnJugar = new JButton("Jugar");
		btnJugar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnJugar.setBounds(165, 101, 147, 25);
		contentPane.add(btnJugar);
		
		btnJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				elegirDificultad();
				actualizarPantalla();
			}
		});
		
		crearBotonSonido();
	}
	
	private void elegirDificultad() {
		JLabel lblDificultad = new JLabel("Elija una dificultad:");
		lblDificultad.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblDificultad.setBounds(190, 208, 118, 21);
		contentPane.add(lblDificultad);
		
		JButton btnFacil = new JButton("Facil");
		btnFacil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jugar(Dificultad.FACIL);
			}
		});
		btnFacil.setBounds(198, 230, 85, 21);
		contentPane.add(btnFacil);
		
		JButton btnMedio = new JButton("Medio");
		btnMedio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jugar(Dificultad.MEDIO);
			}
		});
		btnMedio.setBounds(198, 255, 85, 21);
		contentPane.add(btnMedio);
		
		JButton btnDificil = new JButton("Dificil");
		btnDificil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jugar(Dificultad.DIFICIL);
			}
		});
		btnDificil.setBounds(198, 280, 85, 21);
		contentPane.add(btnDificil);
	}
	
	protected void jugar(Dificultad dificultad) {
		siguienteVentana.jugar(dificultad);
		cambiarVentana();
	}
}
