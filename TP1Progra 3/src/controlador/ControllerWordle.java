package controlador;

import java.awt.Color;
import modelo.Juego;
import recurso.Dificultad;

public class ControllerWordle {

	private Juego juego;
	
	public void Juego(Dificultad dificultad) {
		juego = new Juego(dificultad);
	}

	public String getPalabra() {
		return juego.getPalabra();
	}
	
	public int intentosTotales() {
		return juego.intentosTotales();
	}
	
	public int getLargoDePalabra() {
		return juego.getLargoDePalabra();
	}
	
	public Integer intentos() {
		return juego.cantidadIntentosDisponibles();
	}
	public void guardarRespuesta(String respuesta) {
		juego.guardarRespuesta(respuesta);
	}			
	public boolean existenIntentos() {
		return juego.existenIntentos();
	}
	
	public boolean rtaValida() {
		return juego.rtaEsValida();
	}
	
	public void jugar() {
		juego.turno();
	}
	
	public Color[] coloresDeRta() {
		return juego.mostrarColorLetras();
	}
	
	public boolean resultadoFinal() {
		return juego.gano();
	}
}
