package recurso;

public enum Dificultad {
	FACIL(3,6),
	MEDIO(5,5),
	DIFICIL(7,4);
	
	private int intentosTotales;
	private Integer largoDePalabra;
	
	private Dificultad(Integer largoDePalabra, int intentosTotales) {
		this.largoDePalabra = largoDePalabra;
		this.intentosTotales = intentosTotales;
	}
	
	public Integer getLargoDePalabra() {
		return this.largoDePalabra;
	}
	
	public int intentosTotales() {
		return this.intentosTotales;
	}
}
