package test;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Before;
import org.junit.Test;

import modelo.Juego;
import recurso.Dificultad;

public class JuegoTest {
	
	private Juego wordle;
	private Dificultad dificultad;
	
	@Before
	public void setUp() {
		this.dificultad= Dificultad.MEDIO;
		this.wordle = new Juego(dificultad);
	}
	
	@Test
	public void restarIntentosTest() {
		String respuesta = "jugar";
		int intentos = wordle.cantidadIntentosDisponibles();
		wordle.guardarRespuesta(respuesta);
		wordle.turno();
		
		assertTrue(intentos - 1 == wordle.cantidadIntentosDisponibles());
		}
	
	@Test
	public void guardarRtaTest() {
		String respuesta = "prueba";
		wordle.guardarRespuesta(respuesta);
		
		assertNotNull(wordle.getPalabraUsuario());
	}
	
	@Test
	public void rtaVaciaTest() {
		String respuesta = "";
		wordle.guardarRespuesta(respuesta);
		
		assertFalse(wordle.rtaEsValida());
	}
	
	@Test
	public void rtaInvalidaTest() {
		String respuesta = "poco";
		wordle.guardarRespuesta(respuesta);
		
		assertFalse(wordle.rtaEsValida());
	}
	
	@Test
	public void rtaValidaTest() {
		String respuesta = "juego";
		wordle.guardarRespuesta(respuesta);
		
		assertTrue(wordle.rtaEsValida());
	}
	
	@Test
	public void ganarTest() {
		String respuesta = wordle.getPalabra();
		wordle.guardarRespuesta(respuesta);
		wordle.turno();
		
		assertTrue(wordle.gano());
	}
	
	@Test
	public void perderTest() {
		String respuesta = "ahora";
		wordle.guardarRespuesta(respuesta);
		wordle.turno();
		
		assertFalse(wordle.gano());
	}
	
	@Test
	public void coloresRtaCorrectaTest() {
		String respuesta = wordle.getPalabra();
		Color[] colorRta = new Color[dificultad.getLargoDePalabra()];
		
		for (int i = 0; i < colorRta.length; i++) {
			colorRta[i] = Color.GREEN;
		}
		wordle.guardarRespuesta(respuesta);
		wordle.turno();
		boolean verdad = true;
		Color[] colorWordle = wordle.mostrarColorLetras();
		
		for (int i = 0; i < colorRta.length; i++) {
			verdad = verdad && colorRta[i] == colorWordle[i];
		}
		assertTrue(verdad);
	}
}