package modelo;

import java.awt.Color;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import recurso.*;

public class Juego {

	private Dificultad dificultad;
	private Estado estado;
	
	private String palabra;	
	private char[] palabraChar;
	private int tamañoPalabra;
	private int intentos;
	private List<ColorLetras> colorLetras;
	private boolean palabraEncontrada;
	private String respuestaUsuario;
	
	
	// Se crea el constructor dado la dificultad del juego (FACIL, MEDIO, DIFICIL) dependiendo directamente de eso van los intentos
	public Juego (Dificultad dificultadElegida) {
		this.dificultad = dificultadElegida;
		this.intentos = dificultad.intentosTotales();
		this.palabraEncontrada = false;
		this.colorLetras = new LinkedList<>();
		this.estado = Estado.JUGANDO;
		crearPalabra();
		crearEstructuraPalabra();
		
	}
	
	
	//Toma la palabra al azar dependiendo la dificultad y de ahi su longitud
	private void crearPalabra() {
		this.palabra = elegirPalabraRandom(dificultad);
		this.tamañoPalabra = palabra.length();
		this.palabraChar = new char[tamañoPalabra];
	}
	
	// Crea la estructura de la palabra y se ven los estados de cada letra
	private void crearEstructuraPalabra () {
		for (int i = 0; i < this.tamañoPalabra; i++) {
			this.palabraChar[i] = palabra.charAt(i);
			}
	}
	
	// Obtiene una palabra random de los enum segun la dificultad ( a mayor dificultad mayor es la longitud de la palabra)
	private String elegirPalabraRandom (Dificultad dificultad) {
		Random numeroRandom = new Random ();
		int indicePalabra;
		String respuesta = "";
	
		switch (dificultad) {
		case FACIL :
			PalabraFacil [] palabraFacilTotales = PalabraFacil.values();
			indicePalabra = numeroRandom.nextInt(palabraFacilTotales.length);
			respuesta = palabraFacilTotales [indicePalabra].toString();
			break;
		case MEDIO :
			PalabraMedio [] palabraMedioTotales = PalabraMedio.values();
			indicePalabra = numeroRandom.nextInt(palabraMedioTotales.length);
			respuesta = palabraMedioTotales [indicePalabra].toString();
			break;
		case DIFICIL :
			PalabraDificil [] palabraDificilTotales = PalabraDificil.values();
			indicePalabra = numeroRandom.nextInt(palabraDificilTotales.length);
			respuesta = palabraDificilTotales[indicePalabra].toString();
			break;
		}
		return respuesta;		
	}
	
	// Estados del juego
	public void estado() {
		if(existenIntentos())
			estado = (palabraEncontrada()) ? Estado.GANÓ : Estado.JUGANDO;
		else
			estado =  Estado.PERDIÓ;		
	}
	
	private boolean jugando() {
		return (this.estado.equals(Estado.JUGANDO));
	}
	
	public boolean gano() {
		return (this.estado.equals(Estado.GANÓ) && palabraEncontrada());
	}	
	
	private boolean palabraEncontrada() {
		return palabraEncontrada;
	}
	
	public Color[] mostrarColorLetras() {
		return setearColores();
	}
	
	// Turno
	public void turno() {
		if (rtaEsValida()) {
			vaciarLista();
			// verifica si gano
			if (jugando() && respuestaUsuario.equals(this.palabra)) {
				for (int i = 0; i < tamañoPalabra; i++) {
					colorLetras.add(i, ColorLetras.VERDE);
				}
				palabraEncontrada = true;
				estado();
			}
			// Si el usuario sigue jugando, pero la palabra ingresada no es igual a la palabra a adivinar
			if (jugando()==true && respuestaUsuario!=this.palabra) {
				for (int i = 0; i < tamañoPalabra; i++) {
					String letra = Character.toString(palabraChar[i]);
					
					//Comparar letra por letra la palabra usuario con la palabra a adivinar y ver cual se encuentra en la misma posicion y la guarda en letras encontradas
					if(palabraChar[i]==respuestaUsuario.charAt(i)) {
						colorLetras.add(i,ColorLetras.VERDE);;
					} else {
						// Comparar la letra ingresada por el usuario con la palabra a adivinar y ver cual letra se encuentra en la palabra a adivinar
					    // aunque no sea en la misma posicion
						if (respuestaUsuario.contains(letra)) {
							colorLetras.add(i, ColorLetras.AMARILLO);
						}
						// Se fija si la letra no fue encontrada la guarda en la lista gris.	
						else {
							colorLetras.add(i, ColorLetras.GRIS);
						}
					}
				}
			}
			restarIntento();
			estado();
		}
	}
	
	private Color[] setearColores() {
		Color[] colores = new Color[dificultad.getLargoDePalabra()];
		
		for (int i = 0; i < colorLetras.size(); i++) {
			switch (colorLetras.get(i)) {
			case GRIS:
				colores[i] = Color.GRAY;
				break;
			case AMARILLO:
				colores[i] = Color.YELLOW;
				break;
			case VERDE:
				colores[i] = Color.GREEN;
				break;
			}
		}
		return colores;
	}
	
	private void vaciarLista() {
		colorLetras.removeAll(colorLetras);
	}
	
	// Intentos del juego
	public int cantidadIntentosDisponibles() {
		return this.intentos;
	}
	
	private void restarIntento () {
		this.intentos--;
	}
	
	public boolean existenIntentos() {
		return this.intentos > 0;
	}
	
	public void guardarRespuesta(String rta) {
		this.respuestaUsuario = rta;
	}
	
	public boolean rtaEsValida() {
		return (respuestaUsuario != null && respuestaUsuario.length() == dificultad.getLargoDePalabra());
	}
	
	public String getPalabra() {
		return palabra;
	}	
	
	public String getPalabraUsuario() {
		return respuestaUsuario;
	}	
	
	public int intentosTotales() {
		return dificultad.intentosTotales();
	}
	
	public int getLargoDePalabra() {
		return dificultad.getLargoDePalabra();
	}
}